var playerName = "";

var overState = {
    preload: function(){
        // game.load.image('bgOver', 'assets/over.jpg'); //delete
        // game.load.bitmapFont('carrier_command', 'assets/carrier_command.png', 'assets/carrier_command.xml');//del
    },

    create: function(){
        this.bg = game.add.image(0, 30, 'bgOver');
        this.bg.scale.setTo(1.75, 1.75);

        this.bmpText = game.add.bitmapText(50, 300, 'carrier_command','score: B' + score, 27);
        this.bmpText.inputEnabled = true;
        this.bmpText.input.enableDrag();

        this.nameText = game.add.bitmapText(50, 565, 'carrier_command','name: ', 27);
        this.nameText.inputEnabled = true;
        this.nameText.input.enableDrag();

        myTextArea.style.display = "block";
        btnEnter.style.display = "block";
    }
};

function writetoFirebase(){
    playerName = myTextArea.value;

    if(myTextArea.value != ""){
        //write to firebase
        firebase.database().ref('player').push({
            name: playerName,
            score: score
        }).catch(e => console.log(e.message));

        gotoAgain();
    }
    else{
        alert('Please Enter your Name!');
    }
}

function gotoAgain(){
    game.state.start('again');
}